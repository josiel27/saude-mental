import { User } from '../models/user';




import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { SlidePage } from '../pages/slide/slide';
import { LoginPage } from '../pages/login/login';
import { CadastroPage } from '../pages/cadastro/cadastro';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SonhosPage } from './../pages/sonhos/sonhos';
import { HojePage } from '../pages/hoje/hoje';
import { PrimePage } from './../pages/prime/prime';
import { MeditacaoPage } from './../pages/meditacao/meditacao';
import { TecnicasPage } from './../pages/tecnicas/tecnicas';

import { HttpModule } from '@angular/http';
import { AngularFireModule } from "angularfire2";
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';


import { UserServiceProvider } from '../providers/user-service/user-service';

export const firebaseConfig = {
  apiKey: "AIzaSyCTktxQhwQPpt-ehKcuBOO9yb7_ePCBvuY",
  authDomain: "bloculos-dfec3.firebaseapp.com",
  databaseURL: "https://bloculos-dfec3.firebaseio.com",
  projectId: "bloculos-dfec3",
  storageBucket: "bloculos-dfec3.appspot.com",
  messagingSenderId: "179385888810" 
};



@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    HojePage,
    PrimePage,
    MeditacaoPage,
    TecnicasPage,
    
    TabsPage,
    SlidePage,
    LoginPage,
    CadastroPage,
    SonhosPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(firebaseConfig)
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    HojePage,
    PrimePage,
    MeditacaoPage,
    TecnicasPage,
    TabsPage,
    SlidePage,
    LoginPage,
    CadastroPage,
    SonhosPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserServiceProvider
  ]
})
export class AppModule {}
