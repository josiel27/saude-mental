import { User } from './../models/user';


import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NavController } from 'ionic-angular';

import { HomePage } from '../pages/home/home';
import { HojePage } from '../pages/hoje/hoje';
import { LoginPage } from './../pages/login/login';
import { CadastroPage } from './../pages/cadastro/cadastro';

import { AboutPage } from './../pages/about/about';
import { PrimePage } from './../pages/prime/prime';
import { MeditacaoPage } from './../pages/meditacao/meditacao';
import { TecnicasPage } from './../pages/tecnicas/tecnicas';





import { SonhosPage } from './../pages/sonhos/sonhos';
import { ContactPage } from './../pages/contact/contact';





import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  templateUrl: 'app.html'
})
export class MyApp { 
  
  rootPage: any ;
  pages: [{ title: string, component: any, icon: string }];

  

  @ViewChild('content') navCtrl: NavController;
  @ViewChild(Nav) nav: Nav;

  constructor(platform: Platform,public afAuth: AngularFireAuth, statusBar: StatusBar, splashScreen: SplashScreen, public app: App) {

    const authObserver = afAuth.authState.subscribe( user => {
      if (user) {
        this.rootPage = HomePage;
        authObserver.unsubscribe();
      } else {
        this.rootPage = LoginPage;
        authObserver.unsubscribe();
      }
    });


    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
    this.pages = [      
      { title: 'Início', component: HomePage, icon: "home" },
      { title: 'Apresentação', component: AboutPage, icon: "home" },       
      { title: 'Defina seus sonhos', component: SonhosPage, icon: "home"  },    
      { title: 'Como você se sente hoje', component: HojePage, icon: "home"  },
      { title: 'Conteúdo Prime', component: PrimePage, icon: "home" },
      { title: 'Meditação', component: MeditacaoPage, icon: "home" },
      { title: 'Técnicas evolutivas', component: TecnicasPage, icon: "home" },
      { title: 'Contato', component: ContactPage, icon: "home" },
      
    ];
    
  }

  openPage(page: { title: string, component: any, icon: string }): void {
    this.nav.setRoot(page.component);
  }
  
  logout(){
     
    return this.afAuth.auth.signOut().then( () => this.navCtrl.setRoot(LoginPage));
  }
  
 

}

