
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, AlertController, MenuController, App, IonicPage } from 'ionic-angular';

import { AngularFireAuth } from 'angularfire2/auth';
import { UserServiceProvider } from './../../providers/user-service/user-service';

import { CadastroPage } from './../cadastro/cadastro';
import { HomePage } from './../home/home';

import firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {


  public loginForm: FormGroup;
  public backgroundImage = 'assets/imgs/background/background-5.jpg';

  
  
    constructor(
      private afAuth: AngularFireAuth,
      public navCtrl: NavController,
      public navParams: NavParams,
      public menu: MenuController,
      public formBuilder: FormBuilder,
      public toastCtrl: ToastController,
      public loadingController: LoadingController,
      public alertController: AlertController,     
      public app: App
    ) {
      this.navCtrl = navCtrl;
      this.menu = menu;
      this.menu.enable(false, 'myMenu')
    }
    ngOnInit() {
      this.validarCampos();
    }

    reset() {
      
    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad LoginPage');
    }

    
    validarCampos(): void {
      let emailRegex = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
      this.loginForm = this.formBuilder.group({
        email: ['', Validators.compose([Validators.required, Validators.pattern(emailRegex)])],
        password: ['', [Validators.required, Validators.minLength(6)]],
      })
    }

    login() {
      let loading = this.loadingController.create({
        content: 'Aguarde...'
      });
      loading.present();
      this.afAuth.auth.signInWithEmailAndPassword(this.loginForm.value.email, this.loginForm.value.password)
        .then(auth => {
          loading.dismiss();
          this.navCtrl.setRoot(HomePage);
        })
        .catch(err => {
          loading.dismiss();
          let alert = this.alertController.create({
            title: 'Aviso',
            subTitle: 'Email ou senha incorretos! Tente novamente!',
            buttons: ['OK']
          });
          alert.present();
        });
    }

    isActiveToggleTextPassword: Boolean = true;
    public toggleTextPassword(): void{
        this.isActiveToggleTextPassword = (this.isActiveToggleTextPassword==true)?false:true;
    }
    public getType() {
        return this.isActiveToggleTextPassword ? 'password' : 'text';
    }
  
    goToSignup() {
      
        this.navCtrl.push(CadastroPage);  
     
    }
  
    // Gradient logic from https://codepen.io/quasimondo/pen/lDdrF
    // NOTE: I'm not using this logic anymore, but if you want to use somehow, somewhere,
    // A programmatically way to make a nice rainbow effect, there you go.
    // NOTE: It probably won't work because it will crash your phone as this method is heavy \o/
    colors = new Array(
      [62, 35, 255],
      [60, 255, 60],
      [255, 35, 98],
      [45, 175, 230],
      [255, 0, 255],
      [255, 128, 0]);
  
    step = 0;
    // color table indices for:
    // [current color left,next color left,current color right,next color right]
    colorIndices = [0, 1, 2, 3];
  
    // transition speed
    gradientSpeed = 0.00005;
    gradient = '';
  
    updateGradient() {
  
      const c00 = this.colors[this.colorIndices[0]];
      const c01 = this.colors[this.colorIndices[1]];
      const c10 = this.colors[this.colorIndices[2]];
      const c11 = this.colors[this.colorIndices[3]];
  
      const istep = 1 - this.step;
      const r1 = Math.round(istep * c00[0] + this.step * c01[0]);
      const g1 = Math.round(istep * c00[1] + this.step * c01[1]);
      const b1 = Math.round(istep * c00[2] + this.step * c01[2]);
      const color1 = 'rgb(' + r1 + ',' + g1 + ',' + b1 + ')';
  
      const r2 = Math.round(istep * c10[0] + this.step * c11[0]);
      const g2 = Math.round(istep * c10[1] + this.step * c11[1]);
      const b2 = Math.round(istep * c10[2] + this.step * c11[2]);
      const color2 = 'rgb(' + r2 + ',' + g2 + ',' + b2 + ')';
  
      this.gradient = `-webkit-gradient(linear, left top, right bottom, from(${color1}), to(${color2}))`;
      this.step += this.gradientSpeed;
      if (this.step >= 1) {
        this.step %= 1;
        this.colorIndices[0] = this.colorIndices[1];
        this.colorIndices[2] = this.colorIndices[3];
  
        // pick two new target color indices
        // do not pick the same as the current one
        this.colorIndices[1] =
          (this.colorIndices[1] + Math.floor(1 + Math.random() * (this.colors.length - 1)))
          % this.colors.length;
  
        this.colorIndices[3] =
          (this.colorIndices[3] + Math.floor(1 + Math.random() * (this.colors.length - 1)))
          % this.colors.length;
  
      }
  
      setInterval(() => { this.updateGradient(); }, 40);
    }
  
  }

